/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace EpicTax
{
    public class ApplicationData : Application
    {
        public static bool loginStatus = false;
        public static int CurrentProgressStatus = 0;
    }
}