/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;

namespace EpicTax
{
    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true, ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashActivity : Activity
    {
        System.Timers.Timer timer;
        /// <summary>
        /// Oncreate to start the timer for the splash screen.
        /// </summary>
        /// <param name="bundle"></param>
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            timer = new System.Timers.Timer();
            timer.Interval = 2000;
            timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
            timer.Start();
        }
        /// <summary>
        /// timer_elapsed is the event to stop the timer to exit the splash screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Stop();
            StartActivity(typeof(IntroActivity));
        }
        /// <summary>
        /// Onback key press to terminate application when it is on splash screen.
        /// </summary>
        public override void OnBackPressed()
        {
            base.OnBackPressed();
            timer.Stop();
            Finish();
        }
    }  
}