/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace EpicTax
{
    public class UnderDevelopmentFragment : BaseFragment
    {
        private TextView underDev;
        private Context context;
        public UnderDevelopmentFragment(Context context)
        {
            this.context = context;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        /// <summary>
        /// Oncreateview method to get the controls by creating objects with specific to id's and applying font style.
        /// </summary>
        /// <param name="inflater">Inflater object which is used to inflate our resource xml file into this fragment</param>
        /// <param name="container"></param>
        /// <param name="savedInstanceState"></param>
        /// <returns>view which contains the all the controls of the xml file</returns>
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.UnderDevelopment, container, false);
            underDev = (TextView)view.FindViewById(Resource.Id.underDev_Text);
            underDev.SetTypeface(Helvetica, TypefaceStyle.Normal);
            return view;
        }
    }
}