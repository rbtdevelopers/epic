/**
 * Developer Name: Siva Krishna Alugoju
 * Organization  : Ray Business Technologies pvt ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace EpicTax
{
    public class LoginFragment : BaseFragment
    {
        private TextView headerText, rememberMe, pleaseSignUp;
        private EditText mailAddress, password;
        private Button signUp, signIn;
        Context context;
        public LoginFragment(Context context)
        {
            this.context = context;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState); 
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {            
            var view = inflater.Inflate(Resource.Layout.Login, container, false);
            mailAddress = (EditText)view.FindViewById(Resource.Id.username_et);
            password = (EditText)view.FindViewById(Resource.Id.password_et);
            headerText = (TextView)view.FindViewById(Resource.Id.header_Text);
            rememberMe = (TextView)view.FindViewById(Resource.Id.rememberMe_txt);
            pleaseSignUp = (TextView)view.FindViewById(Resource.Id.haveAccount_txt);
            signUp = (Button)view.FindViewById(Resource.Id.signUp_btn);
            signIn = (Button)view.FindViewById(Resource.Id.signIn_btn);

            signIn.Click += signIn_Click;
            signUp.Click += signUp_Click;

            headerText.SetTypeface(Helvetica, TypefaceStyle.Normal);
            signIn.SetTypeface(Helvetica, TypefaceStyle.Bold);
            signUp.SetTypeface(Helvetica, TypefaceStyle.Bold);
            rememberMe.SetTypeface(Helvetica, TypefaceStyle.Normal);
            mailAddress.SetTypeface(Helvetica, TypefaceStyle.Normal);
            password.SetTypeface(Helvetica, TypefaceStyle.Normal);
            pleaseSignUp.SetTypeface(Helvetica, TypefaceStyle.Normal);
            return view;
        }

        void signIn_Click(object sender, EventArgs e)
        {
            ApplicationData.loginStatus = true;
            LoginWizardScreenOneFragment lgnfrag = new LoginWizardScreenOneFragment(context);
            FragmentTransaction fragTrans = FragmentManager.BeginTransaction();
            fragTrans.Replace(Resource.Id.container, lgnfrag).AddToBackStack("login_wizard_one");
            fragTrans.Commit();
        }

        private void signUp_Click(object sender, EventArgs e)
        {
            RegistrationFragment rfrag = new RegistrationFragment(context);
            FragmentTransaction fragTrans = FragmentManager.BeginTransaction();
            fragTrans.Replace(Resource.Id.container, rfrag).AddToBackStack("registration");
            fragTrans.Commit();
        }
    }
}