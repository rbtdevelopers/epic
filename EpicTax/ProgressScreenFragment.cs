/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace EpicTax
{
    public class ProgressScreenFragment : BaseFragment
    {
        private Context context;
        private RelativeLayout rel;
        private ImageButton closeButton;
        public ProgressScreenFragment(Context context)
        {
            this.context = context;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        /// <summary>
        /// Oncreate method to get the controls by creating objects with specific to id's and applying font style 
        /// and the logic to iterate the progress of the wizard screens.
        /// </summary>
        /// <param name="inflater">Inflater object which is used to inflate our resource xml file into this fragment</param>
        /// <param name="container"></param>
        /// <param name="savedInstanceState"></param>
        /// <returns>view which contains the all the controls of the xml file</returns>
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {    
            var view = inflater.Inflate(Resource.Layout.ProgressScreen, container, false);
            closeButton = (ImageButton)view.FindViewById(Resource.Id.crossBtn);
            closeButton.Click += closeButton_Click;
            TextView txt = (TextView)view.FindViewById(Resource.Id.progressStepsWizardNo);
            txt.Text=ApplicationData.CurrentProgressStatus + " of 15" ;
            if (ApplicationData.CurrentProgressStatus < 4)
            {
                rel = (RelativeLayout)view.FindViewById(Resource.Id.firstRow);
                ColorToProgressSteps(rel);
            }
            else
            {
                if (ApplicationData.CurrentProgressStatus >= 4)
                {
                    rel = (RelativeLayout)view.FindViewById(Resource.Id.firstRow);
                    ColorToProgressSteps(rel);
                    rel = (RelativeLayout)view.FindViewById(Resource.Id.secondRow);
                    ColorToProgressSteps(rel);
                }
                if (ApplicationData.CurrentProgressStatus >= 7)
                {
                    rel = (RelativeLayout)view.FindViewById(Resource.Id.thirdRow);
                    ColorToProgressSteps(rel);
                }
                if (ApplicationData.CurrentProgressStatus >= 10)
                {
                    rel = (RelativeLayout)view.FindViewById(Resource.Id.fourthRow);
                    ColorToProgressSteps(rel);
                }
                if (ApplicationData.CurrentProgressStatus >= 13)
                {
                    rel = (RelativeLayout)view.FindViewById(Resource.Id.fifthRow);
                    ColorToProgressSteps(rel);
                }
            }
                return view;

        }
        /// <summary>
        /// close button click to go back to the previous screen.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void closeButton_Click(object sender, EventArgs e)
        {
            FragmentManager.PopBackStack();
        }
        /// <summary>
        /// applying color to the progress screen step buttons.
        /// </summary>
        /// <param name="relativelyt"></param>
        private void ColorToProgressSteps(RelativeLayout relativelyt)
        {
            for (int i = 0; i < relativelyt.ChildCount; i++)
            {
                TextView txtView = (TextView)relativelyt.GetChildAt(i);
                int textValue=Convert.ToInt32(txtView.Text);
                if ( textValue < ApplicationData.CurrentProgressStatus)
                {
                    if (txtView.Tag.Equals("1"))
                    {
                        txtView.SetBackgroundResource(Resource.Drawable.red_ring);
                    }
                    else
                    {
                        txtView.SetBackgroundResource(Resource.Color.register_btn_color);
                    }
                }
                else if (textValue == ApplicationData.CurrentProgressStatus)
                {
                    if (txtView.Tag.Equals("1"))
                    {
                        txtView.SetBackgroundResource(Resource.Drawable.blue_ring);
                    }
                    else
                    {
                        txtView.SetBackgroundResource(Resource.Color.theme_blue);
                    }
                }
            }
        }
    }
}