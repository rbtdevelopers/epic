/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace EpicTax
{
    public class LoginWizardScreenOneFragment : BaseFragment
    {
        private TextView firstTimeWiz, helloMsg, welcomeMsg, taxReturn, getStarted, langPreference, wizardNumber;
        private Button english, afrikaans, isixhosa, isizulu;
        private Context context;
        private ImageView eSymbol, closeImg;
        private RelativeLayout langMsgCont;
        public LoginWizardScreenOneFragment(Context context)
        {
            this.context = context;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        /// <summary>
        /// Oncreate method to get the controls by creating objects with specific to id's and applying font style and declaring the click events.
        /// </summary>
        /// <param name="inflater">Inflater object which is used to inflate our resource xml file into this fragment</param>
        /// <param name="container"></param>
        /// <param name="savedInstanceState"></param>
        /// <returns>view which contains the all the controls of the xml file</returns>
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ApplicationData.CurrentProgressStatus = 1;
            var view = inflater.Inflate(Resource.Layout.LoginWizardScreenOne, container, false);
            firstTimeWiz = (TextView)view.FindViewById(Resource.Id.firstTimeWizard);
            helloMsg = (TextView)view.FindViewById(Resource.Id.user_name);
            welcomeMsg = (TextView)view.FindViewById(Resource.Id.welcome_text);
            taxReturn = (TextView)view.FindViewById(Resource.Id.taxreturn_text);
            getStarted = (TextView)view.FindViewById(Resource.Id.letStart_txt);
            langPreference = (TextView)view.FindViewById(Resource.Id.langPrefer_text);
            wizardNumber = (TextView)view.FindViewById(Resource.Id.wizard_numbr);
            eSymbol = (ImageView)view.FindViewById(Resource.Id.epictaxEUp_img);
            closeImg = (ImageView)view.FindViewById(Resource.Id.msgClose_img);
            langMsgCont = (RelativeLayout)view.FindViewById(Resource.Id.langMsgContainer);
            english = (Button)view.FindViewById(Resource.Id.english_btn);
            afrikaans = (Button)view.FindViewById(Resource.Id.afrikaans_btn);
            isixhosa = (Button)view.FindViewById(Resource.Id.ishixhosa_btn);
            isizulu = (Button)view.FindViewById(Resource.Id.isizulu_btn);

            english.Click += langbtn_Click;
            afrikaans.Click += langbtn_Click;
            isixhosa.Click += langbtn_Click;
            isizulu.Click += langbtn_Click;
            eSymbol.Click += eSymbol_Click;
            closeImg.Click += closeImg_Click;
            wizardNumber.Click += wizardNumber_Click;

            firstTimeWiz.SetTypeface(Helvetica, TypefaceStyle.Normal);
            helloMsg.SetTypeface(Helvetica, TypefaceStyle.Normal);
            welcomeMsg.SetTypeface(Helvetica, TypefaceStyle.Normal);
            taxReturn.SetTypeface(Helvetica, TypefaceStyle.Normal);
            getStarted.SetTypeface(Helvetica, TypefaceStyle.Normal);
            langPreference.SetTypeface(Helvetica, TypefaceStyle.Normal);
            wizardNumber.SetTypeface(Helvetica, TypefaceStyle.Normal);
            return view;
        }
        /// <summary>
        /// langbtn_click to select the language.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void langbtn_Click(object sender, EventArgs e)
        {
            LoginWizardScreenTwoFragment lwstfrag = new LoginWizardScreenTwoFragment(context);
            FragmentTransaction fragTrans = FragmentManager.BeginTransaction();
            fragTrans.Replace(Resource.Id.container, lwstfrag).AddToBackStack("wizard_screen_two");
            fragTrans.Commit();
        }
        /// <summary>
        /// preffered language message container close button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void closeImg_Click(object sender, EventArgs e)
        {
            langMsgCont.Visibility = ViewStates.Gone;
        }
        /// <summary>
        /// epic e symbol click to display the language preference message container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void eSymbol_Click(object sender, EventArgs e)
        {
            langMsgCont.Visibility = ViewStates.Visible;
        }

        void wizardNumber_Click(object sender, EventArgs e)
        {
            ProgressScreenFragment pgrsfrag = new ProgressScreenFragment(context);
            FragmentTransaction fragTrans = FragmentManager.BeginTransaction();
            fragTrans.Replace(Resource.Id.container, pgrsfrag).AddToBackStack("progress_steps_wizard");
            fragTrans.Commit();
        }
    }
}