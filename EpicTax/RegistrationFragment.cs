/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace EpicTax
{
    public class RegistrationFragment : BaseFragment
    {
        private TextView accept, termsAndCond;
        private EditText firstName, lastName, mailAddress, reTypeMailAddrs, password;
        private Button registerBtn;
        private Context context;
        public RegistrationFragment(Context context)
        {
            this.context = context;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState); 
        }

        /// <summary>
        /// ovverride of OncreateView method to get the controls by creating objects with specific to id's and applying font style.
        /// </summary>
        /// <param name="inflater">Inflater object which is used to inflate our resource xml file into this fragment</param>
        /// <param name="container"></param>
        /// <param name="savedInstanceState"></param>
        /// <returns>view which contains the all the controls of the xml file</returns>
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {            
            var view = inflater.Inflate(Resource.Layout.Registration, container, false);
            firstName = (EditText)view.FindViewById(Resource.Id.firstName_et);
            lastName = (EditText)view.FindViewById(Resource.Id.lastName_et);
            mailAddress = (EditText)view.FindViewById(Resource.Id.mailAddress_et);
            reTypeMailAddrs = (EditText)view.FindViewById(Resource.Id.reTypeMailAddrs_et);
            password = (EditText)view.FindViewById(Resource.Id.password_et);
            accept = (TextView)view.FindViewById(Resource.Id.accept);
            termsAndCond = (TextView)view.FindViewById(Resource.Id.terms);
            registerBtn = (Button)view.FindViewById(Resource.Id.register);

            registerBtn.SetTypeface(Helvetica, TypefaceStyle.Bold);
            firstName.SetTypeface(Helvetica, TypefaceStyle.Normal);
            lastName.SetTypeface(Helvetica, TypefaceStyle.Normal);
            mailAddress.SetTypeface(Helvetica, TypefaceStyle.Normal);
            reTypeMailAddrs.SetTypeface(Helvetica, TypefaceStyle.Normal);
            password.SetTypeface(Helvetica, TypefaceStyle.Normal);
            accept.SetTypeface(Helvetica, TypefaceStyle.Normal);
            termsAndCond.SetTypeface(Helvetica, TypefaceStyle.Normal);
            return view;

        }
    }
}