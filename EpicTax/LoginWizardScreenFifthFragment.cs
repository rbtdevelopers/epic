/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Views.Animations;


namespace EpicTax
{
    public class LoginWizardScreenFifthFragment : BaseFragment
    {
        private TextView secondWiz, quesOne, quesTwo, quesThree, quesFour, quesFive, wizardNumber;
        private Button yesWizQuesFiveButton, noWizQuesFiveButton, yesWizQuesFourButton, noWizQuesFourButton, yesWizQuesThreeButton, noWizQuesThreeButton, yesWizQuesTwoButton, noWizQuesTwoButton, yesWizQuesOneButton, noWizQuesOneButton;
        private Context context;
        private RelativeLayout firstLayout, secondLayout, thirdLayout, fourthLayout, fifthLayout;
        public LoginWizardScreenFifthFragment(Context context)
		{
			this.context = context;
		}
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        /// <summary>
        /// Oncreate method to get the controls by creating objects with specific to id's and applying font style and declaring the click events.
        /// </summary>
        /// <param name="inflater">Inflater object which is used to inflate our resource xml file into this fragment</param>
        /// <param name="container"></param>
        /// <param name="savedInstanceState"></param>
        /// <returns>view which contains the all the controls of the xml file</returns>
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            ApplicationData.CurrentProgressStatus = 5;
            var view = inflater.Inflate(Resource.Layout.LoginWizardScreenFifth, container, false);
            secondWiz = (TextView)view.FindViewById(Resource.Id.wizardName);
            quesOne = (TextView)view.FindViewById(Resource.Id.questionOne);
            quesTwo = (TextView)view.FindViewById(Resource.Id.questionTwo);
            quesThree = (TextView)view.FindViewById(Resource.Id.questionThree);
            quesFour = (TextView)view.FindViewById(Resource.Id.questionFour);
            quesFive = (TextView)view.FindViewById(Resource.Id.questionFive);
            wizardNumber = (TextView)view.FindViewById(Resource.Id.wizardNumbr);

            firstLayout = (RelativeLayout)view.FindViewById(Resource.Id.layoutOne);
            secondLayout = (RelativeLayout)view.FindViewById(Resource.Id.layoutTwo);
            thirdLayout = (RelativeLayout)view.FindViewById(Resource.Id.layoutThree);
            fourthLayout = (RelativeLayout)view.FindViewById(Resource.Id.layoutFour);
            fifthLayout = (RelativeLayout)view.FindViewById(Resource.Id.layoutFive);

            yesWizQuesFiveButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesFive_yes);
            noWizQuesFiveButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesFive_no);

            yesWizQuesFourButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesFour_yes);
            noWizQuesFourButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesFour_no);

            yesWizQuesThreeButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesThree_yes);
            noWizQuesThreeButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesThree_no);

            yesWizQuesTwoButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesTwo_yes);
            noWizQuesTwoButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesTwo_no);

            yesWizQuesOneButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesOne_yes);
            noWizQuesOneButton = (Button)view.FindViewById(Resource.Id.wizTwoQuesOne_no);

            wizardNumber.Click += wizardNumber_Click;

            yesWizQuesFiveButton.Click += yesButton_Click;
            yesWizQuesFourButton.Click += yesButton_Click;
            yesWizQuesThreeButton.Click += yesButton_Click;
            yesWizQuesTwoButton.Click += yesButton_Click;
            yesWizQuesOneButton.Click += yesButton_Click;

            noWizQuesFiveButton.Click += noButton_Click;
            noWizQuesFourButton.Click += noButton_Click;
            noWizQuesThreeButton.Click += noButton_Click;
            noWizQuesTwoButton.Click += noButton_Click;
            noWizQuesOneButton.Click += noButton_Click;

            secondWiz.SetTypeface(Helvetica, TypefaceStyle.Normal);
            quesOne.SetTypeface(Helvetica, TypefaceStyle.Normal);
            quesTwo.SetTypeface(Helvetica, TypefaceStyle.Normal);
            quesThree.SetTypeface(Helvetica, TypefaceStyle.Normal);
            quesFour.SetTypeface(Helvetica, TypefaceStyle.Normal);
            quesFive.SetTypeface(Helvetica, TypefaceStyle.Normal);
            wizardNumber.SetTypeface(Helvetica, TypefaceStyle.Normal);
            return view;
        }
        /// <summary>
        /// yes button click event to make the slide down animation and text,button effects of questions layout.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void yesButton_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            int id = btn.Id;
            Animation slide = AnimationUtils.LoadAnimation(Application.Context, Resource.Animation.slide_down);
            FragmentTransaction fragTrans = FragmentManager.BeginTransaction();
            switch (id)
            {
                case Resource.Id.wizTwoQuesFive_yes:
                    yesWizQuesFiveButton.SetBackgroundResource(Resource.Drawable.yes_selector);
                    yesWizQuesFiveButton.SetTextColor(Color.White);
                    noWizQuesFiveButton.Background = null;
                    noWizQuesFiveButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    LoginWizardScreenSixthFragment lgnwizfrag = new LoginWizardScreenSixthFragment(context);
                    fragTrans.Replace(Resource.Id.container, lgnwizfrag).AddToBackStack("login_wizard_sixth");
                    fragTrans.Commit();
                    break;
                case Resource.Id.wizTwoQuesFour_yes:
                    yesWizQuesFourButton.SetBackgroundResource(Resource.Drawable.yes_selector);
                    yesWizQuesFourButton.SetTextColor(Color.White);
                    noWizQuesFourButton.Background = null;
                    quesFive.SetText(Resource.String.question_yes_five);
                    noWizQuesFourButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    fifthLayout.StartAnimation(slide);
                    fourthLayout.StartAnimation(slide);
                    thirdLayout.StartAnimation(slide);
                    secondLayout.StartAnimation(slide);
                    firstLayout.Visibility = ViewStates.Visible;
                    break;
                case Resource.Id.wizTwoQuesThree_yes:
                    yesWizQuesThreeButton.SetBackgroundResource(Resource.Drawable.yes_selector);
                    yesWizQuesThreeButton.SetTextColor(Color.White);
                    noWizQuesThreeButton.Background = null;
                    quesFour.SetText(Resource.String.question_yes_four);
                    noWizQuesThreeButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    fifthLayout.StartAnimation(slide);
                    fourthLayout.StartAnimation(slide);
                    thirdLayout.StartAnimation(slide);
                    secondLayout.Visibility = ViewStates.Visible;
                    break;
                case Resource.Id.wizTwoQuesTwo_yes:
                    yesWizQuesTwoButton.SetBackgroundResource(Resource.Drawable.yes_selector);
                    yesWizQuesTwoButton.SetTextColor(Color.White);
                    noWizQuesTwoButton.Background = null;
                    quesThree.SetText(Resource.String.question_yes_three);
                    noWizQuesTwoButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    fifthLayout.StartAnimation(slide);
                    fourthLayout.StartAnimation(slide);
                    thirdLayout.Visibility = ViewStates.Visible;
                    break;
                case Resource.Id.wizTwoQuesOne_yes:
                    yesWizQuesOneButton.SetBackgroundResource(Resource.Drawable.yes_selector);
                    yesWizQuesOneButton.SetTextColor(Color.White);
                    noWizQuesOneButton.Background = null;
                    quesTwo.SetText(Resource.String.question_yes_two);
                    noWizQuesOneButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    fifthLayout.StartAnimation(slide);
                    fourthLayout.Visibility = ViewStates.Visible;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// no button click event to make the slide down animation and text,button effects of questions layout.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void noButton_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            int id = btn.Id;
            Animation slide = AnimationUtils.LoadAnimation(Application.Context, Resource.Animation.slide_down);
            FragmentTransaction fragTrans = FragmentManager.BeginTransaction();
            switch (id)
            {
                case Resource.Id.wizTwoQuesFive_no:
                    noWizQuesFiveButton.SetBackgroundResource(Resource.Drawable.no_selector);
                    yesWizQuesFiveButton.Background = null;
                    yesWizQuesFiveButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    noWizQuesFiveButton.SetTextColor(Color.White);
                    LoginWizardScreenSixthFragment lgnwizfrag = new LoginWizardScreenSixthFragment(context);
                    fragTrans.Replace(Resource.Id.container, lgnwizfrag).AddToBackStack("login_wizard_sixth");
                    fragTrans.Commit();
                    break;
                case Resource.Id.wizTwoQuesFour_no:
                    noWizQuesFourButton.SetBackgroundResource(Resource.Drawable.no_selector);
                    yesWizQuesFourButton.Background = null;
                    yesWizQuesFourButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    noWizQuesFourButton.SetTextColor(Color.White);
                    quesFive.SetText(Resource.String.question_no_one);
                    fifthLayout.StartAnimation(slide);
                    fourthLayout.StartAnimation(slide);
                    thirdLayout.StartAnimation(slide);
                    secondLayout.StartAnimation(slide);
                    firstLayout.Visibility = ViewStates.Visible;
                    break;
                case Resource.Id.wizTwoQuesThree_no:
                    noWizQuesThreeButton.SetBackgroundResource(Resource.Drawable.no_selector);
                    yesWizQuesThreeButton.Background = null;
                    yesWizQuesThreeButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    noWizQuesThreeButton.SetTextColor(Color.White);
                    quesFour.SetText(Resource.String.question_no_two);
                    fifthLayout.StartAnimation(slide);
                    fourthLayout.StartAnimation(slide);
                    thirdLayout.StartAnimation(slide);
                    secondLayout.Visibility = ViewStates.Visible;
                    break;
                case Resource.Id.wizTwoQuesTwo_no:
                    noWizQuesTwoButton.SetBackgroundResource(Resource.Drawable.no_selector);
                    yesWizQuesTwoButton.Background = null;
                    yesWizQuesTwoButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    noWizQuesTwoButton.SetTextColor(Color.White);
                    quesThree.SetText(Resource.String.question_yes_three);
                    fifthLayout.StartAnimation(slide);
                    fourthLayout.StartAnimation(slide);
                    thirdLayout.Visibility = ViewStates.Visible;
                    break;
                case Resource.Id.wizTwoQuesOne_no:
                    noWizQuesOneButton.SetBackgroundResource(Resource.Drawable.no_selector);
                    yesWizQuesOneButton.Background = null;
                    yesWizQuesOneButton.SetTextColor(Color.Argb(0xff, 0x00, 0x00, 0xff));
                    noWizQuesOneButton.SetTextColor(Color.White);
                    quesTwo.SetText(Resource.String.question_no_one);
                    fifthLayout.StartAnimation(slide);
                    fourthLayout.Visibility = ViewStates.Visible;
                    break;
                default:
                    break;
            }
        }
        /// <summary>
        /// wizard number click to check the progress of the wizard screens. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void wizardNumber_Click(object sender, EventArgs e)
        {
            ProgressScreenFragment pgrsfrag = new ProgressScreenFragment(context);
            FragmentTransaction fragTrans = FragmentManager.BeginTransaction();
            fragTrans.Replace(Resource.Id.container, pgrsfrag).AddToBackStack("progress_steps_wizard");
            fragTrans.Commit();
        }
    }
}