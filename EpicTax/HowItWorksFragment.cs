/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace EpicTax
{
    public class HowItWorksFragment : BaseFragment
    {
        TextView howWorksQues, lightChatTtl, payServicesTtl, returnFilledTtl, lightChatDesc, payServicesDesc, returnFilledDesc;
        Context context;
        public HowItWorksFragment(Context context)
        {
            this.context = context;
        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState); 
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.HowItWorks, container, false);
            howWorksQues = (TextView)view.FindViewById(Resource.Id.howitworks_ques);
            lightChatTtl = (TextView)view.FindViewById(Resource.Id.lightChat_Title);
            payServicesTtl = (TextView)view.FindViewById(Resource.Id.payServices_Title);
            returnFilledTtl = (TextView)view.FindViewById(Resource.Id.returnFilled_Title);
            lightChatDesc = (TextView)view.FindViewById(Resource.Id.lightChat_Desc);
            payServicesDesc = (TextView)view.FindViewById(Resource.Id.payServices_Desc);
            returnFilledDesc = (TextView)view.FindViewById(Resource.Id.returnFilled_Desc);

            howWorksQues.SetTypeface(Lobster2, TypefaceStyle.Bold);
            lightChatTtl.SetTypeface(Lobster2, TypefaceStyle.Bold);
            lightChatDesc.SetTypeface(Helvetica, TypefaceStyle.Normal);

            payServicesTtl.SetTypeface(Lobster2, TypefaceStyle.Bold);
            payServicesDesc.SetTypeface(Helvetica, TypefaceStyle.Normal);

            returnFilledTtl.SetTypeface(Lobster2, TypefaceStyle.Bold);
            returnFilledDesc.SetTypeface(Helvetica, TypefaceStyle.Normal);
            return view;

        }
        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            
        }
        public override void OnAttach(Activity activity)
        {
            base.OnAttach(activity);        

        }
       
    }
}