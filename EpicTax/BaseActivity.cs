/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Support.V4.App;
using Android.Content.PM;

namespace EpicTax
{
    [Activity(Label = "FontFamily", ScreenOrientation = ScreenOrientation.Portrait)]
    public class BaseActivity : FragmentActivity
    {
        public Typeface Lobster0,Lobster2,Helvetica;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Lobster0 = Typeface.CreateFromAsset(Assets, "Lobster_0.ttf");
            Lobster2 = Typeface.CreateFromAsset(Assets, "LobsterTwo-Regular.ttf");
            Helvetica = Typeface.CreateFromAsset(Assets, "HelveticaNeueLTStd-Lt.otf");
        }
    }
}