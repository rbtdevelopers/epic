/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace EpicTax
{
    public class SubmitQuestionFragment : BaseFragment
    {
        private TextView questionHeading, warning, safety;
        private EditText quesTitle, answer, userName, Email;
        private Button submitQuestion;
        private Context context;
        public SubmitQuestionFragment(Context context)
        {
            this.context = context;
        }
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }
        /// <summary>
        /// OncreateView method to get the controls by creating objects with specific to id's and applying font style.
        /// </summary>
        /// <param name="inflater">Inflater object which is used to inflate our resource xml file into this fragment</param>
        /// <param name="container"></param>
        /// <param name="savedInstanceState"></param>
        /// <returns>view which contains the all the controls of the xml file</returns>
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.SubmitQuestion, container, false);
            quesTitle = (EditText)view.FindViewById(Resource.Id.questionTitle_et);
            answer = (EditText)view.FindViewById(Resource.Id.answer_et);
            userName = (EditText)view.FindViewById(Resource.Id.questionUserName_et);
            Email = (EditText)view.FindViewById(Resource.Id.questionEmail_et);
            questionHeading = (TextView)view.FindViewById(Resource.Id.questionHeading);
            warning = (TextView)view.FindViewById(Resource.Id.warning);
            safety = (TextView)view.FindViewById(Resource.Id.safety);
            submitQuestion = (Button)view.FindViewById(Resource.Id.submitQues);

            submitQuestion.SetTypeface(Helvetica, TypefaceStyle.Bold);
            quesTitle.SetTypeface(Helvetica, TypefaceStyle.Normal);
            answer.SetTypeface(Helvetica, TypefaceStyle.Normal);
            userName.SetTypeface(Helvetica, TypefaceStyle.Normal);
            Email.SetTypeface(Helvetica, TypefaceStyle.Normal);
            questionHeading.SetTypeface(Helvetica, TypefaceStyle.Normal);
            warning.SetTypeface(Helvetica, TypefaceStyle.Normal);
            safety.SetTypeface(Helvetica, TypefaceStyle.Normal);
            return view;
        }
    }
}