﻿/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Graphics;
using Android.Content.PM;

namespace EpicTax
{
    [Activity(Label = "EpicTax", Icon = "@drawable/icon", ScreenOrientation = ScreenOrientation.Portrait)]
	public class IntroActivity : BaseActivity
    {
        private TextView howWorks, epicTax;
        private ImageButton hwItWrksTut, loginScreen, questionScreen, infoScreen, backButton, loginImageButton, logFootHwItWrks, logFootWizAdd, logFootWizRight, logFootWizQues, logFootWizInfo;
        private FragmentTransaction fragTrans;
        private LinearLayout loginFooter, mainFooter;
        /// <summary>
        /// Oncreate method to get the controls by creating objects with specific to id's and applying font style.
        /// </summary>
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            FragmentManager.BackStackChanged += FragmentManager_BackStackChanged;
            SetContentView(Resource.Layout.Intro);
            howWorks = (TextView)FindViewById(Resource.Id.howitworks);
            epicTax = (TextView)FindViewById(Resource.Id.epictax);
            howWorks.SetTypeface(Helvetica, TypefaceStyle.Normal);
            epicTax.SetTypeface(Lobster0, TypefaceStyle.Normal);
            HowItWorksFragment mfrag = new HowItWorksFragment(this);
            fragTrans = FragmentManager.BeginTransaction();
            fragTrans.Add(Resource.Id.container, mfrag);
            fragTrans.Commit();
            hwItWrksTut = (ImageButton)FindViewById(Resource.Id.hwItWrksTut_Btn);
            loginScreen = (ImageButton)FindViewById(Resource.Id.loginScreen_Btn);
            questionScreen = (ImageButton)FindViewById(Resource.Id.contactScreen_Btn);
            infoScreen = (ImageButton)FindViewById(Resource.Id.infoScreen_Btn);
            backButton = (ImageButton)FindViewById(Resource.Id.back);
            loginImageButton = (ImageButton)FindViewById(Resource.Id.login_img_btn);

            logFootHwItWrks = (ImageButton)FindViewById(Resource.Id.loginhwItWrksTut_Btn); 
            logFootWizAdd = (ImageButton)FindViewById(Resource.Id.documentadd_Btn);
            logFootWizRight = (ImageButton)FindViewById(Resource.Id.documentright_Btn);
            logFootWizQues = (ImageButton)FindViewById(Resource.Id.logincontactScreen_Btn);
            logFootWizInfo = (ImageButton)FindViewById(Resource.Id.logininfoScreen_Btn);
            loginFooter = (LinearLayout)FindViewById(Resource.Id.loginfooter);

            mainFooter = (LinearLayout)FindViewById(Resource.Id.footer);

            backButton.Click += backButton_Click;
            hwItWrksTut.Click += operationbuttonclick; 
            loginScreen.Click += operationbuttonclick;
            loginImageButton.Click += loginImageButton_Click;
            questionScreen.Click += operationbuttonclick;
            infoScreen.Click += operationbuttonclick;

            logFootWizAdd.Click += loginFootBtns_Click;
            logFootWizRight.Click += loginFootBtns_Click;
            logFootWizQues.Click += loginFootBtns_Click;
            logFootWizInfo.Click += loginFootBtns_Click;
            logFootHwItWrks.Click += loginFootBtns_Click;

            CommonFooterColor();
            CommonLoginFooterColor();
            hwItWrksTut.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
        }
        /// <summary>
        /// login button click to sign in into the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void loginImageButton_Click(object sender, EventArgs e)
        {                      
            fragTrans = FragmentManager.BeginTransaction();
            LoginFragment lfrag = new LoginFragment(this);
            fragTrans.Replace(Resource.Id.container, lfrag);
            fragTrans.AddToBackStack("login").Commit();
            if (ApplicationData.loginStatus)
            {
                ApplicationData.loginStatus = false;
                loginImageButton.SetImageResource(Resource.Drawable.login);
                mainFooter.Visibility = ViewStates.Visible;
                loginFooter.Visibility = ViewStates.Gone;
            }  
        }
        /// <summary>
        /// back key press functionality
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void backButton_Click(object sender, EventArgs e)
        {
            OnBackPressed();
        }
        /// <summary>
        /// Handling of the backstack when user navigates back.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FragmentManager_BackStackChanged(object sender, EventArgs e)
        {
            string CurrentFragmentName = String.Empty;
            if (FragmentManager.BackStackEntryCount > 0)
            {
                CurrentFragmentName = FragmentManager.GetBackStackEntryAt(FragmentManager.BackStackEntryCount - 1).Name;
            }
            else
            {
                CurrentFragmentName = "how_it_works";
            }
            CommonFooterColor();
            CommonLoginFooterColor();
            loginImageButton.Visibility = ViewStates.Visible;
            switch (CurrentFragmentName)
            {
                case "how_it_works":
                    howWorks.SetText(Resource.String.howit_works);
                    hwItWrksTut.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    logFootHwItWrks.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    break;
                case "login":                    
                    howWorks.SetText(Resource.String.login_text);
                    loginScreen.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    loginImageButton.Visibility = ViewStates.Invisible;
                    mainFooter.Visibility = ViewStates.Visible;
                    loginFooter.Visibility = ViewStates.Gone;
                    break;
                case "registration":
                    howWorks.SetText(Resource.String.registration_text);
                    loginScreen.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    break;
                case "submit_question":
                    howWorks.SetText(Resource.String.questions_text);
                    questionScreen.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    logFootWizQues.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    break;
                case "under_development_info": 
                    infoScreen.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    logFootWizInfo.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    howWorks.SetText(Resource.String.empty_string);
                    break;
				case "under_development_right": 
					logFootWizRight.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    howWorks.SetText(Resource.String.empty_string);
					break;
                case "login_wizard_one": 
                    loginImageButton.SetImageResource(Resource.Drawable.logout);
                    logFootWizAdd.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    howWorks.SetText(Resource.String.newreturn_text);
                    mainFooter.Visibility = ViewStates.Gone;
                    loginFooter.Visibility = ViewStates.Visible;
                    break;
                default:
                    logFootWizAdd.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
                    howWorks.SetText(Resource.String.newreturn_text);
                    break;
            }
        }
        /// <summary>
        /// overriding the onBackPressed event to show the previous screen.
        /// </summary>
        public override void OnBackPressed()
        {
            if ((FragmentManager.BackStackEntryCount > 0))
            {
                if ("how_it_works".Equals(FragmentManager.GetBackStackEntryAt(FragmentManager.BackStackEntryCount - 1).Name))
                {
                    base.OnBackPressed();
                }
                else
                {
                    this.FragmentManager.PopBackStack();
                }
            }
            else
            {
                base.OnBackPressed();
            }
        }

        /// <summary>
        /// back color effect for the footer .
        /// </summary>
        public void CommonFooterColor()
        {           
            hwItWrksTut.SetBackgroundColor(Color.Argb(0xFF,0x49,0x49,0x49));
            loginScreen.SetBackgroundColor(Color.Argb(0xFF, 0x49, 0x49, 0x49));
            questionScreen.SetBackgroundColor(Color.Argb(0xFF, 0x49, 0x49, 0x49));
            infoScreen.SetBackgroundColor(Color.Argb(0xFF, 0x49, 0x49, 0x49));
        }
        /// <summary>
        /// Footer button clicks.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void operationbuttonclick(object sender, EventArgs e)
        {           
            ImageButton btn = sender as ImageButton;
            CommonFooterColor();
            btn.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
            int id = btn.Id;
            fragTrans = FragmentManager.BeginTransaction();
            switch (id)
            {
                case Resource.Id.hwItWrksTut_Btn:
                    FragmentManager.PopBackStackImmediate();
                    HowItWorksFragment mfrag = new HowItWorksFragment(this);
                    fragTrans.Replace(Resource.Id.container, mfrag);
                    fragTrans.AddToBackStack("how_it_works").Commit();
                    break;
                case Resource.Id.loginScreen_Btn:
                    LoginFragment lfrag = new LoginFragment(this);
                    fragTrans.Replace(Resource.Id.container, lfrag);
                    fragTrans.AddToBackStack("login").Commit();
                    break;
                case Resource.Id.contactScreen_Btn:
                    SubmitQuestionFragment sfrag = new SubmitQuestionFragment(this);
                    fragTrans.Replace(Resource.Id.container, sfrag);
                    fragTrans.AddToBackStack("submit_question").Commit();
                    break;
                case Resource.Id.infoScreen_Btn:
                    UnderDevelopmentFragment ufrag = new UnderDevelopmentFragment(this);
                    fragTrans.Replace(Resource.Id.container, ufrag);
                    fragTrans.AddToBackStack("under_development_info").Commit();
                    break;
            }
            fragTrans = null;
        }
        /// <summary>
        /// back color effect for the footer.
        /// </summary>
        public void CommonLoginFooterColor()
        {
            logFootHwItWrks.SetBackgroundColor(Color.Argb(0xFF, 0x49, 0x49, 0x49));
            logFootWizAdd.SetBackgroundColor(Color.Argb(0xFF, 0x49, 0x49, 0x49));
            logFootWizRight.SetBackgroundColor(Color.Argb(0xFF, 0x49, 0x49, 0x49));
            logFootWizQues.SetBackgroundColor(Color.Argb(0xFF, 0x49, 0x49, 0x49));
            logFootWizInfo.SetBackgroundColor(Color.Argb(0xFF, 0x49, 0x49, 0x49));
        }

        /// <summary>
        /// Login footer selections.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loginFootBtns_Click(object sender, EventArgs e)
        {
            ImageButton btn = sender as ImageButton;
            CommonLoginFooterColor();
            btn.SetBackgroundColor(Color.Argb(0xFF, 0x57, 0x57, 0x57));
            int id = btn.Id;
            fragTrans = FragmentManager.BeginTransaction();
            switch (id)
            {
                case Resource.Id.loginhwItWrksTut_Btn:
                    HowItWorksFragment mfrag = new HowItWorksFragment(this);
                    howWorks.SetText(Resource.String.howit_works);
                    fragTrans.Replace(Resource.Id.container, mfrag);
                    fragTrans.AddToBackStack("how_it_works").Commit();
                    break;
                case Resource.Id.documentadd_Btn:
                    LoginWizardScreenOneFragment lfrag = new LoginWizardScreenOneFragment(this);
                    fragTrans.Replace(Resource.Id.container, lfrag);
                    fragTrans.AddToBackStack("login_wizard_add").Commit();
                    howWorks.SetText(Resource.String.newreturn_text);
                    break;
                case Resource.Id.documentright_Btn:
                    UnderDevelopmentFragment udfrag = new UnderDevelopmentFragment(this);
                    fragTrans.Replace(Resource.Id.container, udfrag);
                    fragTrans.AddToBackStack("under_development_right").Commit();
                    break;
                case Resource.Id.logincontactScreen_Btn:
                    SubmitQuestionFragment sfrag = new SubmitQuestionFragment(this);
                    howWorks.SetText(Resource.String.questions_text);
                    fragTrans.Replace(Resource.Id.container, sfrag);
                    fragTrans.AddToBackStack("submit_question").Commit();
                    break;
                case Resource.Id.logininfoScreen_Btn:
                    UnderDevelopmentFragment ufrag = new UnderDevelopmentFragment(this);
                    fragTrans.Replace(Resource.Id.container, ufrag);
                    fragTrans.AddToBackStack("under_development_info").Commit();
                    break;
            }
            fragTrans = null;
        }
    }
}

