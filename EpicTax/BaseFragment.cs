/**
 * Developed by: Siva Krishna Alugoju
 * Organization: Ray Business Technologies Pvt Ltd.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace EpicTax
{
    public class BaseFragment : Fragment
    {
        public Typeface Lobster0, Lobster2, Helvetica;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Lobster0 = Typeface.CreateFromAsset(Application.Context.Assets, "Lobster_0.ttf");
            Lobster2 = Typeface.CreateFromAsset(Application.Context.Assets, "LobsterTwo-Regular.ttf");
            Helvetica = Typeface.CreateFromAsset(Application.Context.Assets, "HelveticaNeueLTStd-Lt.otf");
        }
    }
}